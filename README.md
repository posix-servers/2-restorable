Written by David Wu (DamWu@ucsc.edu) and Rory Landau (rflandau@ucsc.edu).
Orginally written for a class.

# Overview
Httpserver is a simple server that responses to GET and PUT requests for files.
Backup and restoration functionality is available, as detailed below and in DESIGN.pdf.
Its architecture is described in DESIGN.pdf.


# Compiling and Running
Httpserver can and should be compiled using the `make` command.

It can then be run via `./httpserver <domain> [port]`. Port is optional and defaults to 80.

Clients can trigger backup functionality by sending GET requests for /r, /b, and /l.

These calls trigger **r**estore (the most recent backup is used, unless a timestamp is specified with/r/[timestamp]),
**b**ackup (files are backed-up into a timestamped folder),
and **l**ist (list of all available backup timestamps is returned).

# TODO
[ ] Remove 1-multithreaded legacy code. 

# Capabilities and Limitations
* The server makes an explicit execption for 'httpserver' when executing as cannot overwrite itself while running. Without this exception, /r always returns a 500 Server Error that is beyond our control.
* Unable to handle binary files if client requests a PUT and mixes header with file data.

# Style Guide

* Indentation is soft 4.

* Variable declaration, other than explicitly temporary variables, should be performed at the start of the variable's subroutine.

* Lines, ideally, should not exceed 80 characters and definitely no longer than 100 characters.
