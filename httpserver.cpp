#include "Server.hpp"
#include "Lock_Manager.hpp"

/*------------------------------------------------------------------------------
MAIN
------------------------------------------------------------------------------*/
int main (int argc, char** argv) {
    // check for arg count
    if(argc < 2){
        fprintf(stderr, "Usage: httpserver <domain> [port]\n"
        "    Port is optional.\n"
        "    Port 80 is default, but requires superuser permissions\n");
        exit(EXIT_SUCCESS);
    }

    // initalize server class---------------------------------------------------
    Server serv = (argc == 2) ? Server(argv[1]) : Server(argv[1], argv[2]);
}
